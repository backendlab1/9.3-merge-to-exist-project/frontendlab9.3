import type { Role } from './Roles'

type Gender = 'male' | 'female' | 'others'

type User = {
  id?: number
  email: string
  password: string
  fullName: string
  image: string
  gender: Gender // Male, Female, Others
  roles: Role[] // admin, user
}

export type { Gender, User }
